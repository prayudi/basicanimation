//
//  ViewController.swift
//  BasicAnimation
//
//  Created by Prayudi Satriyo on 17/05/19.
//  Copyright © 2019 Prayudi Satriyo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var viewAnimate: UIView!
    @IBOutlet weak var buttonAnimate: UIButton!
    
    @IBAction func buttonHold(_ sender: UIButton) { //KALO DI HOLD FUNCTION NYA AKTIF
        doAnimate()
    }
//    @IBAction func buttonAnimatePressed(_ sender: UIButton) {
//
//    }
    
    var counter = 1
//    var random = Int.random(in: 0...255)
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
        // Do any additional setup after loading the view.
        
    }
    
    func breakAnimate()
    {
        UIView.animate(withDuration: 0.5) {
            self.viewAnimate.alpha = 1 //BUAT ALPHA
            self.viewAnimate.transform = CGAffineTransform(a: 200, b: 200, c: 200, d: 200, tx: 200, ty: 200) //YANG NGEGLITCH
//            self.viewAnimate.transform = CGAffineTransform(scaleX: 20, y: 20) //YANG GA NGEGLITCH
//            self.doAnimate()

    }
    }
    func doAnimate2(){ //LOOPING BUAT OPACITY 100% FADE IN
        UIView.animate(withDuration: 1, delay: 0, options: [], animations: {
            self.viewAnimate.alpha = 1
        }, completion: { (true) in
            self.counter+=1
            print(self.counter)
            self.doAnimate()
        })
        
    }
    
    func doAnimate(){ //LOOPING BUAT OPACITY 0% FADE OUT
        
        UIView.animate(withDuration: 1, delay: 0, options: [], animations: {
            self.viewAnimate.alpha = 0
        }, completion: { (true) in
            self.viewAnimate.backgroundColor = UIColor(red: .random(in: 0...1), green: .random(in: 0...1), blue: .random(in: 0...1), alpha: 1)
            if(self.counter%10 == 0)
            {
                self.breakAnimate()
            }
            else
            {
            self.counter+=1
            print(self.counter)
            self.doAnimate2()
            }
        })
        
    }


}

